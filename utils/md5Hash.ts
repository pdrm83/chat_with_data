import * as CryptoJS from "crypto-js";

/**
 * A function to calculate MD5 hash of input string 
 * 
 * @param {string} input - input string to hash
 * @returns {string} MD5 hash of input
 */
export const calculateMD5 = (input: string): string => {
  return CryptoJS.MD5(input).toString();
}
