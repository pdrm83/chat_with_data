import { ChromaClient } from 'chromadb';
import { OpenAIEmbeddings } from "langchain/embeddings/openai";
import { Chroma } from "langchain/vectorstores/chroma";
import { getDocsFromTextFile } from './document_extractors/get_docs_from_text';
import { getDocsFromPdf } from './document_extractors/get_docs_from_pdf';
import { TextSplitter } from 'langchain/text_splitter';
import { calculateMD5 } from './utils/md5Hash';
import { OpenAIEmbeddingFunction } from 'chromadb';
import { Document } from 'langchain/dist/document';

/**
 * A function to split file into documents and save them along with embeddings in 
 * chroma db. Function uses MD5 hash of documents as id so duplicate upserts are avoided
 * 
 * @param {string} FilePath - .txt or .pdf file
 * @param {TextSplitter} textSplitter - LangChain TextSplitter
 * @param {OpenAIEmbeddingFunction} embedder - embedder used to create Chroma DB collection
 * @param {string} colName - Chroma DB Collection name to document embeddings
 */
export const addFile2VectorStore = async (FilePath: string, textSplitter: TextSplitter,
  embedder: OpenAIEmbeddingFunction, colName: string): Promise<void> => {

  const fileExtension = FilePath.split(".").pop();

  let docs: Document<Record<string, any>>[];

  if (fileExtension && fileExtension.toLowerCase() === 'txt') {
    docs = await getDocsFromTextFile(FilePath, textSplitter);
  }
  else if (fileExtension && fileExtension.toLowerCase() === 'pdf') {
    docs = await getDocsFromPdf(FilePath, textSplitter);
  }
  else {
    docs = [];
    console.log(`No docs created. Check the file extensions. Only .txt and .pdf are supported.`)
  }

  const embeddings = new OpenAIEmbeddings();

  const vectorStore = new Chroma(embeddings, {
    collectionName: colName,
  });

  const client = new ChromaClient({ path: process.env["CHROMA_HOST_ADDRESS"] });
  let numElements;

  let collection = await client.getCollection({ name: colName })
    .then(async (col) => {
      numElements = await col.count();
      console.log(`${numElements} elements already exist in ${colName}`);
      for (const doc of docs) {
        const id = calculateMD5(doc.pageContent);
        const res = await col.get({ ids: [id] });
        if (res.ids.length === 0) {
          await vectorStore.addDocuments([doc], { ids: [id] });
        }
      }
    })
    .catch(async () => {
      console.log('Creating Collection and adding embeddings');
      const ids = docs.map((doc) => calculateMD5(doc.pageContent));
      const collection = await client.createCollection({ name: colName, embeddingFunction: embedder });
      await vectorStore.addDocuments(docs, { ids: ids });
    });
}
