import * as dotenv from 'dotenv';
import { getAIAssistant } from "./get_ai_assistant";
import { HumanMessage, AIMessage } from 'langchain/schema';
import { ConversationalRetrievalQAChain } from 'langchain/chains';

dotenv.config();

const test = async () => {
  const filePaths = ['test_files/us.txt', 'test_files/iran.txt', 'test_files/messi.pdf'];

  const collectionName = "test";

  // Pass file names and chroma collection name
  const aiAssistantWithMemory = await getAIAssistant(filePaths, collectionName, { attachMemory: true });

  // No need to attach the memory when using chat history 
  const aiAssistantWithoutmemory = await getAIAssistant(filePaths, collectionName, { attachMemory: false });

  await runWithMemory(aiAssistantWithMemory);
  await runWithoutMemory(aiAssistantWithoutmemory);

  // Remove the collection here if needed
}

const runWithMemory = async (aiAssistant: ConversationalRetrievalQAChain) => {
  console.log("--------Running AI assistant with memory attached:--------");
  const questions = ["Give me five major events in the history of the US?", "Would you tell me more about the 3rd item?",
    "What are major cities in Iran?", "Who was the last king in Iran?", "Has messi won the world cup?", "when?",
    "Who is Cristiano Ronaldo?"];

  for (const [index, question] of questions.entries()) {
    const res = await aiAssistant.call({ question: question });
    console.log(`Question: ${questions[index]}`);
    console.log(res);
  }
}


const runWithoutMemory = async (aiAssistant: ConversationalRetrievalQAChain) => {
  console.log("--------Running AI assistant without memory attached and with chat history:--------");
  const chatHistory = [new HumanMessage({ content: "who was the last king in Iran?" }),
  new AIMessage({ content: "The last king in Iran was Mohammad Reza Pahlavi." })];
  const question = "What did he accomplish?";
  const res = await aiAssistant.call({ "question": question, chat_history: chatHistory });
  console.log(`Question: ${question}`)
  console.log(res);
}

test();
