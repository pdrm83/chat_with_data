import { ChatOpenAI } from "langchain/chat_models/openai";
import { ConversationalRetrievalQAChain } from "langchain/chains";
import { Chroma } from "langchain/vectorstores/chroma";
import { OpenAIEmbeddings } from "langchain/embeddings/openai";
import { RecursiveCharacterTextSplitter } from "langchain/text_splitter";
import { OpenAIEmbeddingFunction } from 'chromadb';
import { BufferMemory } from "langchain/memory";

import { addFile2VectorStore } from "./add_file_to_vs";
import { CONDENSE_PROMPT } from "./constants/prompts";
import { QA_PROMPT } from "./constants/prompts";

/**
 * A function to get ConversationalRetrievalQAChain based on LLM model "gpt-3.5-turbo"
 * and Chroma vectorStoreRetriever. By default RecursiveCharacterTextSplitter with chunkSize=1000
 * and chunkOverlap=200 is used. You can pass textSplitter of your choice. BufferMemory is
 * used under default condition; attachMemory: true
 * 
 * @param {string []} filePaths - list of .txt and .pdf files
 * @param {string} collectionName - Chroma DB collection name
 * @param {record} options - dictionary to pass attachMemory boolean and textSplitter
 * @returns {ConversationalRetrievalQAChain} aiAssistant 
 */
export const getAIAssistant = async (filePaths: string[], collectionName: string,
  options: Record<string, any> = { attachMemory: true, textSplitter: undefined }) => {

  /* Initialize the LLM to use to answer the question */
  const model = new ChatOpenAI({
    modelName: "gpt-3.5-turbo",
    temperature: 0.2,
  });

  const embeddings = new OpenAIEmbeddings();

  let memory = new BufferMemory({
    memoryKey: 'chat_history',
    inputKey: 'question',
    returnMessages: true
  });

  let textSplitter;

  if (options.textSplitter === undefined) {
    textSplitter = new RecursiveCharacterTextSplitter({ chunkSize: 1000, chunkOverlap: 200 });
  }
  else {
    textSplitter = options.textSplitter;
  }

  const apiKey: string | undefined = process.env["OPENAI_API_KEY"];
  const embedder = new OpenAIEmbeddingFunction({ openai_api_key: apiKey as string });

  for (const filePath of filePaths) {
    await addFile2VectorStore(filePath, textSplitter, embedder, collectionName);
  }

  const vectorStore = new Chroma(embeddings, {
    collectionName: collectionName
  });

  /* Create the chain */
  const aiAssistant = ConversationalRetrievalQAChain.fromLLM(
    model,
    vectorStore.asRetriever(),
    {
      qaTemplate: QA_PROMPT,
      memory: options.attachMemory ? memory : undefined,
      questionGeneratorChainOptions: { template: CONDENSE_PROMPT },
    }
  );

  return aiAssistant;
}
