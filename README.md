# Chat with Data
This is a simple TypeScript repository that helps you build an AI assistant who responds based on the provided data (Retrieval Augmented Generation). The library is built on top of LangChain JS, openAI and Chroma DB libraries. it is particularly useful for JavaScript-based backend frameworks. You can simply provide `.txt` and `.pdf` files to the AI assistant and ask questions against them. Assistant is supposed to not answer questions for which, no context is provided within files. It should simply answer "hmm Im not sure" or ask you to rephrase your question in those scenarios. Lets walk through the steps to set up the assistant:

# Setup and Installation
## Chroma Setup
As opposed to Python, as of now, LangChain JS only works based on docker-based version of Chroma (`client-server` mode), so we need to build the chroma image and run the container to get our vector database running. Please clone [chroma](https://github.com/chroma-core/chroma/tree/main) and run:
```bash
docker-compose up -d --build
```
This will spin up the chroma container and run it on Port **8000**. You can change the port number under [Dockerfile](https://github.com/chroma-core/chroma/blob/main/Dockerfile) in case required. Container must be always up and running once library is being used.

## AI Assistant Setup
First of all, you need to acquire an `OpenAI_API_KEY` from [openai](https://openai.com/) in order to leverage OpenAI LLM models.

Create a `.env` file under the root directory of this project and add the following environment variables
```
OPENAI_API_KEY=<api_key>

# please change if needed
CHROMA_HOST_ADDRESS="http://localhost:8000"
```
Run `npm install` to install dependencies.

## Test installation
Run `npm test` to make sure `test_ai_assistant.ts` runs and everything works. At the end of the test, you must be seeing a list of questions followed by answers popping up. Sample files are fed from `test_files`. This test may take a few minutes to run first time since documents and embeddings are just added to Chroma DB collection but in practice, communicating with vecotor db is fast once embeddings are added. As a result of this test, a persisting collection called `test` is added to Chroma container. Just so you know, I am not removing the Chroma collection at the end of the tests so you can keep playing if needed.

## How to Use
There are two modes you can use the AI assistant; with or without memory attached. In the memory-attached mode, LLM takes care of filling out the `chat_history` in `CONDENSE_PROMPT` based on user's questions and AI's answers, where as in no memory-attached mode, `chat_history` is passed as a list to Langchain. Check out `test_ai_assistant.ts` for more usage details. By default, `RecursiveCharacterTextSplitter` instance is being used to make documents from files, however you can pick other splitters and pass to `getAIAssistant` method. Please checkout [Langchain document transformers](https://js.langchain.com/docs/modules/data_connection/document_transformers/) for more. I will provide a brief walkthrough of the essential steps required to utilize both modes effectively."

### Memory-attached Mode
```javascript
import * as dotenv from 'dotenv';
import { getAIAssistant } from "./get_ai_assistant";

dotenv.config();

const collectionName = "CollectionName";
const filePaths = ["</path/to/file_1.txt>", ... "</path/to/file_n.pdf>"]

const aiAssistantWithMemory = await getAIAssistant(filePaths, collectionName, { attachMemory: true }); // pass preferred textSplitter to options if needed
const res = await aiAssistant.call({ question: "<your question here>" });
```

### Chat History Mode
```javascript
import * as dotenv from 'dotenv';
import { HumanMessage, AIMessage } from 'langchain/schema';
import { getAIAssistant } from "./get_ai_assistant";

dotenv.config();

const collectionName = "CollectionName";
const filePaths = ["</path/to/file_1.txt>", ... "</path/to/file_n.pdf>"]

const chatHistory = [new HumanMessage({ content: "<firstQuestion>" }),
  new AIMessage({ content: "<firstAnswer>" }), new HumanMessage({ content: "<secondQuestion>" }),
  new AIMessage({ content: "<secondAnswer>" }), ...];

const question = "<your question here>";

const aiAssistantWithoutmemory = await getAIAssistant(filePaths, collectionName, { attachMemory: false });
const res = await aiAssistant.call({ "question": question, chat_history: chatHistory });
```
