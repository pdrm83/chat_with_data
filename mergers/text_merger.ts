import * as fs from 'fs';

/**
 * This method takes a list of text files and provides the combination of contents in text format
 * which can be written to a file later
 * 
 * @param {string []} filePaths - path to text files
 * @returns {string} - string representation of text files being attached with a separating new line
 */
export const appendTextFiles = (filePaths: string[]): string => {
  try {
    let combinedContent = '';

    // Read the content of each file and append them together
    for (const filePath of filePaths) {
      const fileContent: string = fs.readFileSync(filePath, 'utf-8');
      combinedContent = combinedContent + '\n' + fileContent;
    }

    return combinedContent;
  } catch (error) {
    console.error('Error appending files', error);
  }
  return '';
}
