import { PDFDocument } from "pdf-lib";
import * as fs from 'fs';

/**
 * This function is used to make a byte array from a list of pdf files. Byte array
 * can be saved to a pdf file.
 * 
 * @param {string []} paths - path to pdf files
 * @returns {Uint8Array} Byte array representing combinatio of multiple pdf files
 */
export const pdfMerger = async (paths: string[]) => {
    const mergedPdf = await PDFDocument.create();

    for (const filePath of paths) {
        const pdf = await PDFDocument.load(fs.readFileSync(filePath));

        const copiedPages = await mergedPdf.copyPages(pdf, pdf.getPageIndices());
        copiedPages.forEach((page) => mergedPdf.addPage(page));
    }

    const mergedPdfFile = await mergedPdf.save();
    return mergedPdfFile;
}
