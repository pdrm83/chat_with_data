import { PDFLoader } from "langchain/document_loaders/fs/pdf";
import { TextSplitter } from 'langchain/text_splitter';

/**
 * A function to split a pdf file to chunks that can be saved to vector store
 * 
 * @param {string} pdfFilePath - path to .pdf file
 * @param {TextSplitter} textSplitter - TextSplitter used to break the pdf file contents to chunks
 * @returns {Document<Record<string, any>> []} 
 */
export const getDocsFromPdf = async (pdfFilePath: string, textSplitter: TextSplitter) => {
  const loader = new PDFLoader(pdfFilePath, {
    // you may need to add `.then(m => m.default)` to the end of the import
    pdfjs: () => import("pdfjs-dist/legacy/build/pdf.js"),
  });
  const docs = await loader.loadAndSplit(textSplitter);
  // docs[i].metadata.loc.lines returns lines from source file
  return docs;
}
