import * as fs from 'fs';
import { TextSplitter } from 'langchain/text_splitter';

/**
 * A function to get a promise which resolves to constituent documents in the .txt file
 * 
 * @param {string} filePath - path to .txt file
 * @param {TextSplitter} textSplitter - TextSplitter used to break the .txt file to chunks
 * @returns 
 */
export const getDocsFromTextFile = async (filePath: string, textSplitter: TextSplitter): Promise<any[]> => {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, "utf8", async (err, data) => {
      if (err) {
        reject(err);
        return;
      }

      try {
        const docs = await textSplitter.createDocuments([data], [{ "source": filePath }]);
        resolve(docs);
      } catch (error) {
        reject(error);
      }
    });
  });
};

export const getDocsFromText = async (text: string, textSplitter: TextSplitter) => {
  const docs = await textSplitter.createDocuments([text]);
  return docs;
}
